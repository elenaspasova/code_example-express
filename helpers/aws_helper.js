/*
 *
 * Aws upload/delete helper
 *
 * */

var AWS = require('aws-sdk');
var config = require('../config/config');
var fs = require('fs');

AWS.config.update({
    region: 'us-west-2',
    credentials: new AWS.Credentials({
        accessKeyId: config.aws_access_key,
        secretAccessKey: config.aws_secret
    })
});

var s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    params: {Bucket: config.s3_bucket_name}
});


var aws_helper = {

    uploadImage: function (tmp_path, remotePath, callback) {
        var bodystream = fs.createReadStream(tmp_path);
        s3.upload({
            Key: remotePath,
            Body: bodystream,
            ACL: 'public-read',
            ContentType: 'image/jpeg'
        }, function (err, data) {
            if (err) return callback(err, null);

            callback(null, true);
        });
    },

    deleteImage: function (path, callback) {
        console.log('bit', config.s3_bucket_name, path);
        var params = {
            Bucket: config.s3_bucket_name,
            Key: path
        };
        s3.deleteObject(params, function (err, data) {
            if (err) return callback(err, null); // an error occurred

            callback(null, true);
        });
    }

};

module.exports = aws_helper;
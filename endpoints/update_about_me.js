var express = require('express');
var router = express.Router();
var users = require('../models/users');
var async = require('async');
var authHelper = require('../helpers/auth_helper');


router.post('/', function (req, res) {

    var aboutMe = req.body.Object.aboutMe;
    if (!aboutMe) {
        return res.json({success: false, message: 'Missing obligatory data!'});
    }

    var updateUserData = function (callback) {
        users.findByIdAndUpdate(req.decoded._id, {
            description: aboutMe
        }, function (err, data) {
            if (err) return callback(err, null);

            authHelper.updateMemcachedUser(req.theToken, data._id, function (err, an) {
                if (err) return callback(err, null);
                callback(null, true);
            });
        })
    };

    //ACTUAL LOGIC

    async.series([
            updateUserData
        ],
        function (err, results) {
            if (err) {
                console.log(err);
                return res.json({success: false, message: 'Error updating about me!'});
            }

            res.json({success: true, message: 'About Me info updated!'});
        }
    );

});

module.exports = router;

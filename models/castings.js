var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');
var users = require('./users');
var regions = require('./regions');

mongoose.Promise = require('q').Promise;

var CastingsSchema = new mongoose.Schema({
    closed: {type: Boolean, default: false, index: true},
    closedAt: Date,
    deadlineEmailSent: {type: Boolean, default: false, index: true},
    loosingSelftapesWarningEmailSent: {type: Boolean, default: false, index: true},
    loosingSelftapesWarningEmailSent2: {type: Boolean, default: false, index: true},
    selftapesExpire: {type: Date, index: true},
    selftapesDeleted: {type: Boolean, default: false, index: true},
    castingExtended: [{
        onDate: Date,
        amount: Number
    }],
    deletedCall: {type: Boolean, index: true, default: false},
    option: String,
    privacyModel: String,
    headline: {type:String,index:true},
    message: String,
    typeOfJob: String,
    productionDate: Date,
    productionDateTo: Date,
    callClicks: {type: Number, default: 0},
    callClickUser:[{type: mongoose.Schema.Types.ObjectId, ref: 'users'}],
    castingDeadline: Date,
    productionType: [{type: mongoose.Schema.Types.ObjectId, ref: 'production_types'}],
    filter: {
        age: [{type: Number, index: true}],
        weight: [{type: Number, index: true}],
        height: [{type: Number, index: true}],
        hairColor: [{type: Number, index: true}],
        eyeColor: [{type: Number, index: true}],
        gender: {type: String, index: true},
        nationality: [{type: mongoose.Schema.Types.ObjectId, ref: 'regions'}],
        region: [{type: String, index: true}],
        union: {type: String, index: true}
    },
    documents: [
        {
            realName: String,
            path: String
        }
    ],
    cookie: String,
    location: String,
    role: String,
    email: String,
    compensation: String,
    directions: String,
    other: String,
    chosenActors: [{type: mongoose.Schema.Types.ObjectId, ref: 'users'}],
    director: {type: mongoose.Schema.Types.ObjectId, ref: 'users'},
    approved: {type: Boolean, default: false},
    declined: {type: Boolean, index: true},
    client: {type: String, index: true},
    applications: [
        {
            user: {type: mongoose.Schema.Types.ObjectId, ref: 'users'},
            selftape: String,
            deviceInfo:[{
                user: {type: mongoose.Schema.Types.ObjectId, ref: 'users'},
                videoId:String,
                info:{}
            }],
            selftapeDescription: String,
            lastUpdated: {type: Date, index: true},
            writtenResponse: String,
            completed: {type: Boolean, index: true, default: false},
            conversation: [{
                who: String,
                message: String,
                viewedDirector: Boolean,
                viewedActor: Boolean,
                sent_at: Date
            }],
            vote: {
                value: String,
                voter: String
            }
        }
    ]

});

CastingsSchema.plugin(timestamps);
module.exports = mongoose.model('castings', CastingsSchema);